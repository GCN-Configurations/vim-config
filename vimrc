call pathogen#runtime_append_all_bundles()
call pathogen#helptags()

execute pathogen#infect()

" setting of vim environment, and calling plugins

set relativenumber
colorscheme nord
syntax on
filetype plugin indent on

set tabstop=4
set softtabstop=4

set t_Co=256
set autoindent

" mapping, and setting shortcuts
set pastetoggle=<F3>

map <C-n> :NERDTreeToggle<CR>
map <C-t><left> :tabprev<cr>
map <C-t><right> :tabnext<cr>
map <C-t><up> :tabnew<cr>
map <C-t><down> :q<cr>
