# Vim Configuration

This repository holds my vim configuration.

# Installation:

```bash
# Clone Repository
$ git clone https://gitlab.com/GCN-Configurations/vim-config.git ~/.vim

# Create symbolic link to vimrc
$ ln -s ~/.vim/vimrc ~/.vimrc

# Install Plugins
$ git submodule update --init --recursive
```

# Features

- Theme & Color Scheme
- FileTree
- Relative Line Number

# Usage

Adding Submodules :

	git submodule add link-to-repo.git bundle/git-name

# Shortcuts

|Keys|Shortcut|
|----|--------|
|F3|Toggles Paste Mode|
|Ctrl+n|Toggles Nerd Tree|
|Ctrl+t+up|Creates a tab|
|Ctrl+t+down|Closes a tab|
|Ctrl+t+left|Goes to Previous tab|
|Ctrl+t+right|Goes to Next tab|
